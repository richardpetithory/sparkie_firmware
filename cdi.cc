#include <Arduino.h>

#define SPARK_OUTPUT_PIN 0
#define HALL_PIN 0 // Interrupt pin 0 = digital 2 on the UNO
#define SPARK_PIN 9
#define SPARK_DWELL_TIME 100
#define MANUAL_DELAY_PIN 0
#define MAGNET_COUNT 2
#define DEGREES_PER_MAGNET 180
#define MANUAL_ADJUST_FREEDOM 20
#define UI_UPDATE_FREQUENCY 200 // in milliseconds
#define INITIAL_ADVANCE 0

// MAX_RPM must be evenly divisible by CURVE_GRANULARITY
#define MAX_RPM 15000
#define CURVE_GRANULARITY 100
#define POINT_COUNT MAX_RPM / CURVE_GRANULARITY

#define MAX_COORDINATES 20
#define BEZIER_WEIGHT 40

struct Coordinate {
    int RPM;
    float degrees;
};

unsigned long g_last_ui_update = 0;
float g_pulses_since_ui_update = 0;
unsigned long g_last_pulse = 0;
float g_manual_delay = 1;
unsigned long g_last_degree_time = 0;
int g_RPM;
int g_total_delay = 0;
float g_status = 0;

float g_points[POINT_COUNT];

void setup();
void pulse();
void loop();
void fire_spark();
float lookup_current_delay();
int plot_points(struct Coordinate *coordinates);
void directPinMode(int pinNumber, int mode);
void directDigitalWrite(int pinNumber, int mode);

void setup() {
    Serial.begin(115200);

    struct Coordinate coordinates[MAX_COORDINATES] = {{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1}};

    coordinates[0].RPM = 0;
    coordinates[0].degrees = 1.0;
    coordinates[1].RPM = 3000;
    coordinates[1].degrees = 1.0;
    coordinates[2].RPM = 7000;
    coordinates[2].degrees = 1.0;
    coordinates[3].RPM = 10000;
    coordinates[3].degrees = 10.0;
    coordinates[4].RPM = 14000;
    coordinates[4].degrees = 1.0;

    plot_points(coordinates);

    directPinMode(SPARK_PIN, OUTPUT);

    attachInterrupt(HALL_PIN, pulse, CHANGE);

    Serial.println("CDI Started");
}

void loop() {
    unsigned long ui_update_duration;
    unsigned long now_time;

    now_time = millis();

    ui_update_duration = now_time - g_last_ui_update;

    if (ui_update_duration > UI_UPDATE_FREQUENCY) {
        Serial.print("rpm:");
        Serial.println(g_RPM);

        g_manual_delay = MANUAL_ADJUST_FREEDOM * ((float)(analogRead(MANUAL_DELAY_PIN) + 1) / 1024) * MANUAL_ADJUST_FREEDOM;

        g_last_ui_update = now_time;
        g_pulses_since_ui_update = 0;
    }
}

void pulse() {
    static int pulses = 0;
    unsigned long now_time = micros();
    // unsigned int manual_delay = 0;
    float curve_delay = 0;
    float delay_fix_time = 0;
    float one_degree_time;
    float pulse_rotation_time = 0;

    pulses++;
    g_pulses_since_ui_update++;

    if (now_time < g_last_pulse) {
        // This means the micros() has overflowed and rolled over.
        g_last_pulse = 4294967295 - g_last_pulse;
    }

    if ((pulses == MAGNET_COUNT) & (g_last_pulse != 0)) {
        pulse_rotation_time = now_time - g_last_pulse;

        g_last_pulse = now_time;

        one_degree_time = pulse_rotation_time / DEGREES_PER_MAGNET;

        g_RPM = (int)(60000000 / (one_degree_time * 360));

        curve_delay = lookup_current_delay() * one_degree_time;

        // manual_delay = (int)((one_degree_time / MANUAL_ADJUST_FREEDOM) * g_manual_delay) + 1;

        delay_fix_time = (1 - (g_RPM / (float)MAX_RPM)) * (one_degree_time * 20.0);

        g_status = delay_fix_time;

        //g_total_delay = 1 + curve_delay + manual_delay + delay_fix_time + (one_degree_time - ((one_degree_time + g_last_degree_time) / 2));

        delayMicroseconds(curve_delay);

        fire_spark();

        pulses = 0;

        g_last_degree_time = one_degree_time;
    } else {
        g_last_pulse = now_time;
    }
}

void fire_spark() {
    directDigitalWrite(SPARK_PIN, HIGH);

    delayMicroseconds(SPARK_DWELL_TIME);

    directDigitalWrite(SPARK_PIN, LOW);
}

int plot_points(struct Coordinate *coordinates) {
    int count;
    int coord_count = 0;

    int delta_counter;

    float delta;
    float t, T;
    int x;
    float y;

    for (coord_count = 0; (coordinates[coord_count].RPM != -1) & (coord_count < MAX_COORDINATES); coord_count++) {
        coordinates[coord_count].RPM = coordinates[coord_count].RPM / CURVE_GRANULARITY;
        //coordinates[coord_count].degrees = (int)(400 - (100 * coordinates[coord_count].degrees));
    }

    // Make sure we actually have a valid lower bound
    coordinates[0].RPM = 0;

    // Make sure we have a valid upper bound
    if (coordinates[coord_count - 1].RPM < POINT_COUNT) {
        coordinates[coord_count] = coordinates[coord_count - 1];
        coord_count++;
        coordinates[coord_count - 1].RPM = POINT_COUNT;
    }

    struct Coordinate current, next;

    for (count = 0; count < coord_count; count++) {
        current = coordinates[count];

        if (count == (coord_count - 1)) {
            next = coordinates[count];
        } else {
            next = coordinates[count + 1];
        }

        delta = 1.0 / (int)(POINT_COUNT);

        t = 0;

        for (delta_counter = 0; delta_counter < POINT_COUNT; delta_counter++) {
            t += delta;

            T = 1 - t;

            int curve_strength = (int)((next.RPM - current.RPM) / 4);

            x = current.RPM * T * T * T + 3 * t * T * T * (current.RPM + curve_strength) + 3 * t * t * T * (next.RPM - curve_strength) + t * t * t * next.RPM;
            y = current.degrees * T * T * T + 3 * t * T * T * current.degrees + 3 * t * t * T * next.degrees + t * t * t * next.degrees;

            g_points[x] = y;
        }
    }

    return true;
}

float lookup_current_delay() {
    int current_point_position = g_RPM / CURVE_GRANULARITY;
    float current = g_points[current_point_position];
    float next =  g_points[current_point_position + 1];

    return current + ((next - current) * ((g_RPM % CURVE_GRANULARITY) / CURVE_GRANULARITY));
}

void directPinMode(int pinNumber, int mode) {
    volatile unsigned char* target;

    if (pinNumber < 8) {
        target = &DDRD;
    } else {
        target = &DDRB;

        pinNumber -= 8;
    }

    if (mode) {
        *target |= 1 << pinNumber;
    } else {
        *target &= ~(1 << pinNumber);
    }
}

void directDigitalWrite(int pinNumber, int mode) {
    volatile unsigned char* target;

    if (pinNumber < 8) {
        target = &PORTD;
    } else {
        target = &PORTB;

        pinNumber -= 8;
    }

    if (mode) {
        *target |= 1 << pinNumber;
    } else {
        *target &= ~(1 << pinNumber);
    }
}
